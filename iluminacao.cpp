#include <stdlib.h>
#include <stdio.h>
#include <GL/glut.h>
#include <time.h>

#define SENS_ROT	10.0
#define SENS_OBS	1.0
#define SENS_TRANSL	1.0

#define TAM 1000
#define D 100

int x_ini,y_ini,bot;
GLfloat rotX, rotY, rotX_ini, rotY_ini;
GLfloat obsX, obsY=200, obsZ=1000, obsX_ini, obsY_ini, obsZ_ini;
GLfloat fAspect = 1, angle = 45;

GLfloat luzAmbiente[4]={0.2,0.2,0.2,1.0};
GLfloat luzDifusa[4]={0.7,0.7,0.7,1.0};		 // "cor"
GLfloat luzEspecular[4]={1.0, 1.0, 1.0, 1.0};// "brilho"
GLfloat posicaoLuz[4]={0.0, 30.0, 120.0, 1.0};
int tamanhoMaximo = 400;

//desenho da serpente = 'd' down,'u' = up,'r' = right ,'l'=left
char* serpente= new char[tamanhoMaximo];
int sizeOfSerpente = 0;
int iteractions = 60;
float tamanhoEsferaSerpente = 36.0F;

//posicao da cabe�a da serpente
GLfloat posXCabecaSerpente=-999+tamanhoEsferaSerpente,posZCabecaSerpente=-999+tamanhoEsferaSerpente;
//movimento da cabe�a
float movX = 0,movZ = 0;

unsigned char lastKey = 'w';
unsigned char penultKey = 's';
//verifica se foi precionado uma tecla para um lado inv�lido
bool teclaValida = false;

//verifica se absorveu uma comida
bool absorveu = false;

//posicao da serpente, acima do  ch�o
//(tamanhoEsferaSerpente � o raio da esfera para ficar exatamente acima do ch�o)
float posYserp= -60+ tamanhoEsferaSerpente;
float zComida,xComida;

//verifica se a cobra bateu ou n�o
bool bateu = false;
bool parouJogo = false;

void geraComida();
void verificaPosicao();

void PosicionaObservador(void)
{
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	//glTranslatef(-obsX,-obsY,-obsZ); //Outra opcao de camera
	glRotatef(rotX,1,0,0);
	glRotatef(rotY,0,1,0);

	gluLookAt(obsX,obsY,obsZ, obsX -obsX_ini,0.0,obsZ-obsZ_ini, 0.0,1.0,0.0);
}

// Fun��o usada para especificar o volume de visualiza��o
void EspecificaParametrosVisualizacao(void)
{
	// Especifica sistema de coordenadas de proje��o
	glMatrixMode(GL_PROJECTION);
	// Inicializa sistema de coordenadas de proje��o
	glLoadIdentity();

	// Especifica a proje��o perspectiva
	gluPerspective(angle,fAspect,0.4,5000);

	// Especifica posi��o do observador e do alvo
	PosicionaObservador();
}

void DesenhaChao()
{
	//Flags para determinar a cord de cada quadrado
	bool flagx, flagz;
	//Define a normal apontando para cima
	glNormal3f(0,1,0);

	glBegin(GL_QUADS);
	flagx = false;
	//X varia de -TAM a TAM, de D em D
	for(float x=-TAM; x<TAM; x+=D)
	{
		//Flagx determina a cor inicial
		if(flagx) flagz = false;
		else flagz = true;
		//Z varia de -TAM a TAM, de D em D
		for (float z=-TAM;z<TAM;z+=D)
		{
			//Escolhe cor
			if(flagz)
				glColor3f(0,1.0,0);
			else
				glColor3f(0,1.0,0);
			//E desenha o quadrado
			glVertex3f(x,-60,z);
			glVertex3f(x+D,-60,z);
			glVertex3f(x+D,-60,z+D);
			glVertex3f(x,-60,z+D);
			//Alterna cor
			flagz = !flagz;
		}
		//A cada coluna, alterna cor inicial
		flagx = !flagx;
	}
	glEnd();

}

void DefineIluminacao()
{
	// Capacidade de brilho do material
	GLfloat especularidade[4]={1.0,1.0,1.0,1.0};
	GLint especMaterial = 60;

	// Habilita o modelo de coloriza��o de Gouraud
	glShadeModel(GL_SMOOTH);

	// Define a reflet�ncia do material
	glMaterialfv(GL_FRONT,GL_SPECULAR, especularidade);
	// Define a concentra��o do brilho
	glMateriali(GL_FRONT,GL_SHININESS,especMaterial);

	// Ativa o uso da luz ambiente
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, luzAmbiente);

	// Define os par�metros da luz de n�mero 0
	glLightfv(GL_LIGHT0, GL_AMBIENT, luzAmbiente);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, luzDifusa );
	glLightfv(GL_LIGHT0, GL_SPECULAR, luzEspecular );
	glLightfv(GL_LIGHT0, GL_POSITION, posicaoLuz );

}

//verifica se a cobra bateu em si mesma
void verificaSeEnroscou(){

}

void DesenhaSerpente(){

    verificaPosicao();
    glBegin(GL_SPHERE_MAP);

    if(!teclaValida){
     if(lastKey== 'w'){
        movX = 0;
        movZ = tamanhoEsferaSerpente;
     }else if(lastKey== 's'){
        movX = 0;
        movZ = -tamanhoEsferaSerpente;
     } else if(lastKey== 'a'){
        movX = tamanhoEsferaSerpente;
        movZ = 0;
     } else if(lastKey== 'd'){
        movX = -tamanhoEsferaSerpente;
        movZ = 0;
     }
    }

    float posXAux = posXCabecaSerpente;
    float posZAux = posZCabecaSerpente;

    glPushMatrix();
    glColor3f(0.0f, 0.0f, 1.0f);
    glTranslatef( posXAux, posYserp, posZAux );
    glutSolidSphere( tamanhoEsferaSerpente, 850, 810 );
    glPopMatrix();

    if(sizeOfSerpente > 0){

        for(int i =tamanhoMaximo-sizeOfSerpente;i<tamanhoMaximo;i++){
        int index1 = 1;

        if(serpente[i] == 'd'){
            posZAux-=tamanhoEsferaSerpente*2;
        }else if(serpente[i] == 'u'){
            posZAux+=tamanhoEsferaSerpente*2;
        }else if(serpente[i] == 'a'){
            posXAux-=tamanhoEsferaSerpente*2;
        }else if(serpente[i] == 'd'){
            posXAux+=tamanhoEsferaSerpente*2;
        }
        glPushMatrix();
        glLoadIdentity();
        glColor3f(0.0f, 0.0f, 1.0f);
        glTranslatef( posXAux, posYserp, posZAux );
        glutSolidSphere( tamanhoEsferaSerpente, 50, 110 );
        glPopMatrix();
        }
    }

    glEnd();

}

void geraComida(){
    if(iteractions ==0 || absorveu){
        iteractions = 60;
        srand(rand());
        zComida = static_cast <float> (rand() %2000 - 1000);
        srand(rand());
        xComida = static_cast <float> (rand() %2000 - 1000);
        absorveu = false;
    }
    glPopMatrix();
    glPushMatrix();
    glColor3f(0.0f, 0.0f, 1.0f);
    glTranslatef( xComida, posYserp, zComida );
    glutSolidSphere( tamanhoEsferaSerpente, 850, 810 );



}

//verifica posicao da serpente (se bateu na parede ou se absorveu alguma comida)
void verificaPosicao(){

    int posAux = tamanhoMaximo-sizeOfSerpente;

    //verifica se bateu na parede
    if(posXCabecaSerpente + tamanhoEsferaSerpente > TAM ||
       posXCabecaSerpente - tamanhoEsferaSerpente < -TAM ||
       posZCabecaSerpente + tamanhoEsferaSerpente > TAM ||
       posZCabecaSerpente - tamanhoEsferaSerpente < -TAM){
        bateu = true;
    }else if(posXCabecaSerpente  > xComida - tamanhoEsferaSerpente &&
             posXCabecaSerpente  < xComida + tamanhoEsferaSerpente){
        if(posZCabecaSerpente  > zComida - tamanhoEsferaSerpente &&
             posZCabecaSerpente  < zComida + tamanhoEsferaSerpente){
                 absorveu = true;
            if(lastKey== 'w'){
                serpente[posAux] = 'd';
            }else if(lastKey== 's'){
                serpente[posAux] = 'u';
            } else if(lastKey== 'a'){
                serpente[posAux] = 'r';
            } else if(lastKey== 'd'){
                serpente[posAux] = 'l';
            }
            posXCabecaSerpente=xComida;
            posZCabecaSerpente=zComida;
            sizeOfSerpente+=1;

        }
    }

}
// Fun��o callback chamada para fazer o desenho
void Desenha(void)
{
    if(!bateu){
	// Limpa a janela e o depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	EspecificaParametrosVisualizacao();

	DesenhaChao();

    DesenhaSerpente();

    geraComida();

    glutPostRedisplay();

    glutSwapBuffers();
    }
    if(bateu && !parouJogo ){
         MessageBox(NULL, "Fim de jogo", "Bateu", MB_OK | MB_ICONEXCLAMATION);
         parouJogo =true;
    }
    srand(time(NULL));
    iteractions--;
}

void GeraMovimento(int te){
    if(!bateu) {
       posXCabecaSerpente+=movX;
       posZCabecaSerpente+=movZ;
    }
    glutTimerFunc(100,GeraMovimento,1);
}
// Inicializa par�metros de rendering
void Inicializa (void)
{
    sizeOfSerpente=0;
    posXCabecaSerpente=-50+tamanhoEsferaSerpente;
    posZCabecaSerpente=-50+tamanhoEsferaSerpente;
    iteractions = 60;
    obsX_ini = obsX;
	obsY_ini = obsY;
    obsZ_ini = obsZ;
	// Especifica que a cor de fundo da janela ser� preta
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	// Habilita a defini��o da cor do material a partir da cor corrente
	glEnable(GL_COLOR_MATERIAL);
	//Habilita o uso de ilumina��o
	glEnable(GL_LIGHTING);
	// Habilita a luz de n�mero 0
	glEnable(GL_LIGHT0);
	// Habilita o depth-buffering
	glEnable(GL_DEPTH_TEST);

	PosicionaObservador();
	glutPostRedisplay();
}

// Fun��o callback chamada quando o tamanho da janela � alterado
void AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
	// Para previnir uma divis�o por zero
	if ( h == 0 ) h = 1;

	// Especifica o tamanho da viewport
	glViewport(0, 0, w, h);

	// Calcula a corre��o de aspecto
	fAspect = (GLfloat)w/(GLfloat)h;

	EspecificaParametrosVisualizacao();
}

// Fun��o callback chamada para gerenciar eventos do mouse
void GerenciaMouse(int button, int state, int x, int y)
{
	if(state==GLUT_DOWN)
	{
		x_ini = x;
		y_ini = y;
		obsX_ini = obsX;
		obsY_ini = obsY;
		obsZ_ini = obsZ;
		rotX_ini = rotX;
		rotY_ini = rotY;
		bot = button;
	}
	else bot = -1;
}

void GerenciaMovim(int x, int y)
{
	if(bot==GLUT_LEFT_BUTTON)
	{
		int deltax = x_ini - x;
		int deltay = y_ini - y;

		rotY = rotY_ini + deltax/SENS_ROT;
		rotX = rotX_ini + deltay/SENS_ROT;
	}

	else if(bot==GLUT_RIGHT_BUTTON)
	{
		int deltaz = y_ini - y;

		obsZ = obsZ_ini - deltaz/SENS_OBS;
	}

	else if(bot==GLUT_MIDDLE_BUTTON)
	{
		int deltax = x_ini - x;
		int deltay = y_ini - y;

		obsX = obsX_ini - deltax/SENS_TRANSL;
		obsY = obsY_ini + deltay/SENS_TRANSL;
	}
}

void GerenciaTeclado(unsigned char key,int,int)
{
    teclaValida = false;
	if (key == 27) {
        exit(0);
	}
    //penultKey = lastKey;
    if(key == 'w'){
        if(sizeOfSerpente == 1 ||
           (sizeOfSerpente != 1 && lastKey != 's')){

            movX = 0;
            movZ = -tamanhoEsferaSerpente;
            penultKey = lastKey;
            lastKey = key;
            teclaValida = true;

        }
    }else if(key == 's'){
        if(sizeOfSerpente == 1 ||
           (sizeOfSerpente != 1 && lastKey != 'w')){
            movX = 0;
            movZ = tamanhoEsferaSerpente;
            penultKey = lastKey;
            lastKey = key;
            teclaValida = true;
        }
    }else if(key == 'a'){
        if(sizeOfSerpente == 1 ||
           (sizeOfSerpente != 1 && lastKey != 'd')){
            movX = -tamanhoEsferaSerpente;
            movZ = 0;
            penultKey = lastKey;
            lastKey = key;
            teclaValida = true;
        }
    }else if(key == 'd'){
        if(sizeOfSerpente == 1 ||
           (sizeOfSerpente != 1 && lastKey != 'a')){
            movX = tamanhoEsferaSerpente;
            movZ = 0;
            penultKey = lastKey;
            lastKey = key;
            teclaValida = true;
        }
    } else if (key == 'r') {
        bateu = false;
        parouJogo = false;
        Inicializa();
        Desenha();
    }

}

void GerenciaTecladoEspecial(int key, int x,int y)
{
    if(key == GLUT_KEY_DOWN){
        obsZ= obsZ +25;
    }
    else if(key == GLUT_KEY_UP){
        obsZ= obsZ -25;
    }
    else if(key == GLUT_KEY_LEFT){
        obsX= obsX -25;
    }
    else if(key == GLUT_KEY_RIGHT){
        obsX= obsX +25;
    }
}

// Programa Principal
int main(void)
{
    int argc = 0;
	char *argv[] = { (char *)"gl", 0 };

	glutInit(&argc,argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(800,600);
	glutCreateWindow("Trabalho 2 - Snake");
	glutDisplayFunc(Desenha);
	glutTimerFunc(100,GeraMovimento,1);
	glutReshapeFunc(AlteraTamanhoJanela);
	glutMotionFunc(GerenciaMovim);
	glutMouseFunc(GerenciaMouse);
	glutKeyboardFunc(GerenciaTeclado);
	glutKeyboardUpFunc(GerenciaTeclado);
	glutSpecialFunc(GerenciaTecladoEspecial);
	DefineIluminacao();
	Inicializa();
	glutMainLoop();

	return 0;
}
